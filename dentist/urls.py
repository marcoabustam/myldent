from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static
from .views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('website.urls')),
    path('index_odonto/', index, name='index_odonto'),
    path('tinymce', include('tinymce.urls')),
    path('pacientes/', include('pacientes.urls')),
    path('', include('authentication.urls')),
    path('agenda/', include('schedule.urls')),
    path('reportes/', include('reports.urls')),
    # equivalente a odontograma records
    path('records/', include('odontogram.urls')),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
