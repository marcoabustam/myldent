from django.urls import path
from . import views

urlpatterns = [
    path('', views.upload_report, name="report"),
    path('', views.events_within_date_range, name='events_within_date_range'),

]

