from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from .models import SalaryReport
from datetime import date

def events_within_date_range(request):
    start_date = date(2023, 1, 1)
    end_date = date(2023, 12, 31)
    
    events = SalaryReport.objects.filter(date__range=(start_date, end_date))
    print(events)
    return render(request, 'reports.html', {'events': events})

@login_required
def upload_report(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        print(myfile)
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        return render(request, 'reports.html', {'uploaded_file_url': uploaded_file_url})
    return render(request, 'reports.html')

