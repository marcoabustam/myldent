from django.db import models
from django.db import models
from django.utils import timezone

# Create your models here.
class SalaryReport(models.Model):
    start_date = models.DateField(default=timezone.now, null=True, blank=True, verbose_name='Fecha Desde')
    end_date = models.DateField(null=True, blank=True, verbose_name='Fecha Hasta')