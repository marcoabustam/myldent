from django.db import models
from django.utils import timezone
from allergies.models import Allergies
from diseases.models import Diseases
from datetime import date
from tinymce import models as tinymce_models
from ckeditor.fields import RichTextField
# from django.forms import models
# from django.forms.fields import ChoiceField

# Create your models here.
class Paciente(models.Model):
    
    GENDER = (
    ('', 'Seleccione Género'),
    ('Hombre', 'Hombre'),
    ('Mujer', 'Mujer'),
)
    PREVITION = (
        ('', 'Seleccione una Previsión'),
        ('Fonasa', 'Fonasa'),
        ('Colmena Golden Cross','Colmena Golden Cross'),
        ('Banmédica','Banmédica'),
        ('Consalud','Consalud'),
        ('CruzBlanca','CruzBlanca'),
        ('Vida Tres','Vida Tres'),
        ('Nueva Más Vida','Nueva Más Vida'),
        ('Chuquicamata','Chuquicamata'),
        ('Río Blanco','Río Blanco'),
        ('San Lorenzo','San Lorenzo'),
        ('Masvida','Masvida'),
        ('Isalud Codelco','Isalud Codelco'),
        ('Cruz del Norte','Cruz del Norte'),
        ('Fundación Banco Estado','Fundación Banco Estado'),
        ('Escencial','Escencial'),
        ('Particular','Particular'),
         )
    
    
    name = models.CharField(max_length=50, null=True, blank=True, verbose_name='Nombre')
    rut = models.CharField(max_length=50, null=True, blank=True, verbose_name='Rut')
    email = models.CharField(max_length=50, null=True, blank=True, verbose_name='email')
    phone = models.CharField(max_length=12, null=True, blank=True, verbose_name='Telefono')
    address = models.CharField(max_length=12, null=True, blank=True, verbose_name='Dirección')
    date_of_birth = models.DateField(null=True, blank=True, verbose_name='Fecha Nacimiento')
    age = models.IntegerField(editable=False, null=True, blank=True, verbose_name="Edad")
    gender = models.CharField(verbose_name='Sexo', choices=GENDER, max_length=20, null=True, blank=True)
    is_active = models.BooleanField(default=True, verbose_name='¿Paciente Activo?')
    prevition = models.CharField(verbose_name='Previsión', choices=PREVITION,  max_length=30, null=True, blank=True)
    diseases = models.ForeignKey(Diseases,on_delete=models.CASCADE, null=True, blank=True, verbose_name='Enfermedades')
    allergies = models.ForeignKey(Allergies,on_delete=models.CASCADE, null=True, blank=True, verbose_name='Alergias')
    medications = models.CharField(max_length=30, null=True, blank=True, verbose_name='Medicamentos')
    last_control = models.DateField(null=True, blank=True, verbose_name='Fecha Último Control')
    create_date = models.DateField(default=timezone.now, verbose_name="Fecha de Ingreso Paciente")
    update_date = models.DateField(default=timezone.now, verbose_name="Fecha de Actualización")
    body = RichTextField(null=True, blank=True)
    date_body_save = models.DateField(null=True, blank=True, verbose_name='Fecha Evolución')

    class Meta: 
            verbose_name = "Paciente"
            verbose_name_plural = "Pacientes"
    
    def __str__(self):
        return str(self.name) or ""
    
    def age_calculate(self):
        today = date.today()
        delta = today - self.date_of_birth
        self.age = delta.days // 365 
    
    def save(self, *args, **kwargs):
        self.age_calculate()
        super().save(*args, **kwargs)

