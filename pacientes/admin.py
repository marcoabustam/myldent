from django.contrib import admin
from .models import Paciente

class PacienteAdmin(admin.ModelAdmin):
    list_display=('name','rut','phone', 'age','address','last_control','date_of_birth','is_active','create_date','update_date') 
    search_fields=('name','rut','phone', 'age', 'address','last_control','date_of_birth','is_active','create_date','update_date')
    list_filter=('name','rut','phone', 'age', 'address','last_control','date_of_birth','is_active','create_date','update_date')

admin.site.register(Paciente, PacienteAdmin)
