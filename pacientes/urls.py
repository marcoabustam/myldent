from django.urls import path, include
from . import views

urlpatterns = [
     path('', views.index, name="index"),
     path('delete_px/<int:patient_id>', views.delete_patient, name='delete_patient'),
     path('edit_patient/<int:patient_id>', views.edit_patient, name='edit_patient'),
     path('patient_detail/<int:patient_id>', views.patient_detail, name='patient_detail'),
     path('create_patient/', views.create_patient, name='create_patient'),
     path('register_patient/', views.register_patient, name='register_patient'),
     path('generar_pdf/<int:patient_id>', views.generar_pdf, name='generar_pdf'),
     path('evolutions_save/<int:patient_id>', views.evolutions_save, name='evolutions_save'),
]
