from io import BytesIO
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .models import Paciente
from django.contrib import messages
from .forms import *
from django.http import FileResponse, JsonResponse
from django.shortcuts import render
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas


# Create your views here.
@login_required
def index(request):
    pacientes = Paciente.objects.filter(is_active=True).order_by('id')
    return render(request, "index.html", {"pacientes":pacientes})

@login_required
def delete_patient(request, patient_id):
    patient = get_object_or_404(Paciente, pk=patient_id)
   
    patient.delete()
    created_message = f"Paciente {patient.name} eliminado exitosamente."
    messages.add_message(request, messages.SUCCESS, created_message)
    return redirect('index')

@login_required
def create_patient(request):
    form = PatientForm
    return render(request, 'create_patient.html', {'form': form})

@login_required
def register_patient(request):
    if request.method == 'POST':
        form = PatientForm(request.POST)
        if form.is_valid():
            patient = form.save()
            created_message = f"Paciente {patient.name} creado exitosamente."
            messages.add_message(request, messages.SUCCESS, created_message)
            return redirect('index') 
    else:
        messages.add_message(request, messages.ERROR, f"La orden no pudo ser creada.")
        form = PatientForm()

    return render(request, 'create_patient.html', {'form': form})

@login_required
def patient_detail(request, patient_id):
    patient = Paciente.objects.get(id=patient_id)
    patient_form_detail = DetailPatientForm(instance=patient)
    evolution_form = EvolutionForm()
    if request.method == 'POST':
        return True
        # form = EvolutionForm(request.POST)
        # if form.is_valid():
        #     print("entro")

    else:
        evolution_form = EvolutionForm()

    return render(request, "patient_detail.html", {"paciente":patient, "form":patient_form_detail, "evo_form":evolution_form})

@login_required
def evolutions_save(request, patient_id):
    patient = Paciente.objects.get(id=patient_id, is_active=True)
    patient_form = PatientForm(request.POST or None, instance=patient)
    evolution_form = EvolutionForm()
    if request.method == 'POST':
        texto = request.POST.get("texto")
        return JsonResponse({'texto':texto})

    return render(request, "patient_detail.html", {"paciente":patient, "texto":texto})

@login_required
def edit_patient(request, patient_id):
    patient = Paciente.objects.get(id=patient_id, is_active=True)
    patient_form = PatientForm(request.POST or None, instance=patient)
    if request.method == 'POST':
        if patient_form.is_valid():
            patient_form.save()
            created_message = f"Paciente {patient.name} editado exitosamente."
            messages.add_message(request, messages.SUCCESS, created_message)
            return redirect('index') 
    return render(request, "edit_patient.html", {"form":patient_form})

def pdf_view(request, patient_id):
    patient = Paciente.objects.get(id=patient_id, is_active=True)
    patient_form = PatientForm(request.POST or None, instance=patient)

        
    return render(request, "pdf_generate.html", {"form":patient_form})

@login_required
def generar_pdf(request, patient_id):
    buffer = BytesIO()
    p = canvas.Canvas(buffer)

    # Obtener los datos de tu modelo Django. Reemplaza esto con tus datos reales.
    patient = Paciente.objects.get(id=patient_id, is_active=True)

    # Agregar texto al PDF.
    p.drawString(100, 750, f"Nombre: {patient.name}")
    p.drawString(100, 730, f"RUT: {patient.rut}")
    p.drawString(100, 710, f"Teléfono: {patient.phone}")
    p.drawString(100, 690, f"Edad: {patient.age}")
    p.drawString(100, 670, f"Dirección: {patient.address}")
    p.drawString(100, 650, f"Último control: {patient.last_control}")
    p.drawString(100, 630, f"Fecha de Nacimiento: {patient.date_of_birth}")
    p.drawString(100, 630, f"Fecha de Nacimiento: {patient.body}")

    p.showPage()
    p.save()

    # Preparar el objeto FileResponse con el contenido del PDF.
    buffer.seek(0)
    response = FileResponse(buffer, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="mi_archivo.pdf"'

    return response
