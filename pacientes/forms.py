import datetime
from django import forms
from .models import Paciente
from diseases.models import Diseases
from allergies.models import Allergies
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Submit, Field, Div
from tinymce.widgets import TinyMCE
from ckeditor.widgets import CKEditorWidget

class PatientForm(forms.ModelForm):
    def today():
        return datetime.date.today().strftime('%Y-%m-%d')
    
    class Meta:
        model = Paciente  
        fields = ['name', 'rut', 'email', 'gender', 'phone', 'address', 'last_control', 'date_of_birth', 'create_date', 'diseases', 'allergies','prevition']
        exclude = ['is_active','update_date']
    
    GENDER = (
    ('', 'Seleccione Género'),
    ('Hombre', 'Hombre' ),
    ('Mujer', 'Mujer'),
)
    PREVITION = (
        ('', 'Seleccione una Previsión'),
        ('Fonasa', 'Fonasa'),
        ('Colmena Golden Cross','Colmena Golden Cross'),
        ('Banmédica','Banmédica'),
        ('Consalud','Consalud'),
        ('CruzBlanca','CruzBlanca'),
        ('Vida Tres','Vida Tres'),
        ('Nueva Más Vida','Nueva Más Vida'),
        ('Chuquicamata','Chuquicamata'),
        ('Río Blanco','Río Blanco'),
        ('San Lorenzo','San Lorenzo'),
        ('Masvida','Masvida'),
        ('Isalud Codelco','Isalud Codelco'),
        ('Cruz del Norte','Cruz del Norte'),
        ('Fundación Banco Estado','Fundación Banco Estado'),
        ('Escencial','Escencial'),
        ('Particular','Particular'),
         )

    name = forms.CharField(label="Nombre cliente", widget=forms.TextInput(attrs={'class': 'form-control'}))
    rut = forms.CharField(label="Rut", widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.CharField(label="Email", widget=forms.TextInput(attrs={'class': 'form-control'}))
    gender = forms.ChoiceField(label="Sexo", choices=GENDER, widget=forms.Select(attrs={'class': 'form-control'}))
    phone = forms.CharField(label="Teléfono", widget=forms.TextInput(attrs={'class': 'form-control'}))
    address = forms.CharField(label="Domicilio", widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_control = forms.DateField(label="Fecha último Control", initial=today(), widget=forms.DateInput(format=('%Y-%m-%d'), attrs={'type':'date', 'class': 'font-size:2px;'}))
    date_of_birth = forms.DateField(label="Fecha de Nacimiento", widget=forms.DateInput(format=('%Y-%m-%d'), attrs={'type':'date','class': 'form-control'}))
    create_date = forms.DateField(label="Fecha de Ingreso Paciente", initial=today(), widget=forms.DateInput(format=('%Y-%m-%d'), attrs={'type':'date','class': 'form-control'}))
    diseases = forms.ModelChoiceField(label="Enfermedad/es", queryset=Diseases.objects.all(), empty_label='Seleccione una Enfermedad')
    allergies = forms.ModelChoiceField(label="Alergia/s", queryset=Allergies.objects.all(), empty_label='Seleccione una Alergia')
    prevition = forms.ChoiceField(label='Previsión',  choices=PREVITION, widget=forms.Select(attrs={'class': 'form-control'}))

    # medications = models.CharField(max_length=30, null=True, blank=True, verbose_name='Medicamentos')
    
    # @property
    # def helper(self):
    #     helper = FormHelper()
    #     helper.form_tag = False # This is crucial.
    #     helper.layout = Layout(
    #         Div(
    #             Div('name', css_class='col-md-3',),
    #             Div('rut', css_class='col-md-3',),
    #             Div('phone', css_class='col-md-3'),
    #             Div('address', css_class='col-md-3'),
    #             css_class='row'
    #         ),
    #         Div(
    #             Div('last_control', css_class='col-md-3',),
    #             Div('date_of_birth', css_class='col-md-3'),
    #             Div('create_date', css_class='col-md-3'),
    #             Div('email', css_class='col-md-3',),
    #             css_class='row'
    #         ),
    #         Div(
    #             Div('prevition', css_class='col-md-3'),
    #             Div('gender', css_class='col-md-3'),
    #             Div('diseases', css_class='col-md-3'),
    #             Div('allergies', css_class='col-md-3'),
    #             css_class='row'
    #         ),
    #     )

    #     return helper

class DetailPatientForm(forms.ModelForm):

    class Meta:
        model = Paciente
        fields = ['name', 'rut', 'email', 'gender', 'phone', 'address', 'last_control', 'date_of_birth', 'create_date', 'diseases', 'allergies','prevition']
        exclude = ['is_active','update_date']
    
    GENDER = {
    '': 'Seleccione Género',
    'H': 'Hombre' ,
    'M': 'Mujer',
    }

    GENDER_CHOICES = (
    ('', 'Seleccione Género'),
    ('hombre', 'Hombre'),
    ('mujer', 'Mujer'),
    )
    PREVITION = (
        ('', 'Seleccione una Previsión'),
        ('Fonasa', 'Fonasa'),
        ('Colmena Golden Cross','Colmena Golden Cross'),
        ('Banmédica','Banmédica'),
        ('Consalud','Consalud'),
        ('CruzBlanca','CruzBlanca'),
        ('Vida Tres','Vida Tres'),
        ('Nueva Más Vida','Nueva Más Vida'),
        ('Chuquicamata','Chuquicamata'),
        ('Río Blanco','Río Blanco'),
        ('San Lorenzo','San Lorenzo'),
        ('Masvida','Masvida'),
        ('Isalud Codelco','Isalud Codelco'),
        ('Cruz del Norte','Cruz del Norte'),
        ('Fundación Banco Estado','Fundación Banco Estado'),
        ('Escencial','Escencial'),
        ('Particular','Particular'),
         )

    name = forms.CharField(label="Nombre Paciente", widget=forms.TextInput(attrs={'class': 'form-control'}))
    age = forms.CharField(label="Edad", widget=forms.TextInput(attrs={'class': 'form-control'}))
    rut = forms.CharField(label="Rut", widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.CharField(label="Email", widget=forms.TextInput(attrs={'class': 'form-control'}))
    gender = forms.ChoiceField(label="Sexo", choices=GENDER, widget=forms.Select(attrs={'class': 'form-control'}))
    phone = forms.CharField(label="Teléfono", widget=forms.TextInput(attrs={'class': 'form-control'}))
    address = forms.CharField(label="Domicilio", widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_control = forms.DateField(label="Fecha último Control", widget=forms.DateInput(format=('%Y-%m-%d'), attrs={'type':'form-control datetimepicker-input', 'class': 'font-size:20px;'}))
    date_of_birth = forms.DateField(label="Fecha de Nacimiento", widget=forms.DateInput(format=('%Y-%m-%d'), attrs={'type':'form-control datetimepicker-input','class': 'form-control'}))
    create_date = forms.DateField(label="Fecha de Ingreso Paciente", widget=forms.DateTimeInput(format='%d/%m/%Y', attrs={'class': 'form-control datetimepicker-input', 'readonly':'readonly'}))
    diseases = forms.ModelChoiceField(label="Enfermedad/es", queryset=Diseases.objects.all(), empty_label='Seleccione una Enfermedad')
    allergies = forms.ModelChoiceField(label="Alergia/s", queryset=Allergies.objects.all(), empty_label='Seleccione una Alergia')
    prevition = forms.ChoiceField(label='Previsión',  choices=PREVITION, widget=forms.Select(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        instanceAttr = getattr(self, 'instance', None)
        instance = kwargs.get('instance')
        if instanceAttr and instance.pk and instance:
            self.fields['age'].initial = instance.age
            for fields in self.fields:
                self.fields[fields].widget.attrs['readonly'] = True
                if self.fields[fields].widget.input_type == 'checkbox':
                    self.fields[fields].widget.attrs['onclick'] = 'return false;'

class EditPatientForm(forms.ModelForm):

    class Meta:
        model = Paciente
        fields = '__all__'

    name = forms.CharField(label="Nombre cliente", widget=forms.TextInput(attrs={'class': 'form-control'}))
    rut = forms.CharField(label="Rut", widget=forms.TextInput(attrs={'class': 'form-control'}))
    phone = forms.CharField(label="Teléfono", widget=forms.TextInput(attrs={'class': 'form-control'}))
    address = forms.CharField(label="Domicilio", widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_control = forms.DateField(label='Fecha Último Control', input_formats=['%d/%m/%Y'])
    date_of_birth = forms.DateField(label="Fecha de Nacimiento", widget=forms.TextInput(attrs={'class': 'form-control'}))
    is_active = forms.BooleanField(label='¿Paciente Activo?')
    create_date = forms.DateField(label="Fecha de Creación")
    update_date = forms.DateField(label="Fecha de Actualización")

class EvolutionForm(forms.Form):
    def today():
        return datetime.date.today().strftime('%Y-%m-%d')     
      
    class Meta:
        model = Paciente
        fields = ['text']
    
    body = forms.CharField(widget=CKEditorWidget(attrs={'cols': 180, 'rows': 130, 'class': 'mi-body'}))
    date_body_save = forms.DateField(label="Fecha Historial Guardado", initial=today(), widget=forms.DateInput(format=('%d-%m-%Y'), attrs={'type':'date', 'class': 'font-size:2px;', 'style':'!important'}))


    def __init__(self, *args, **kwargs):
        super(EvolutionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('grabar', 'Grabar'))