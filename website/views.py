from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .forms import CustomUserForm
from pacientes.models import Paciente 
from pacientes.forms import DetailPatientForm

@login_required
def base(request):
    return render(request, 'base.html', {})


@login_required
def home(request):
    return render(request, 'home.html', {})


@login_required
def view_profile(request):
    user = User.objects.get(id=request.user.id)
    form = CustomUserForm(instance=user)

    return render(request, 'view_profile.html', {'form': form})

@login_required
def pagination(request, patient_id):
    patient = Paciente.objects.get(id=patient_id)
    patient_form_detail = DetailPatientForm(instance=patient)

    return render(request, "navigation.html", {"paciente":patient, "form":patient_form_detail})

@login_required
def patient_detail(request, patient_id):
    patient = Paciente.objects.get(id=patient_id)
    patient_form_detail = DetailPatientForm(instance=patient)

    return render(request, "patient_detail.html", {"paciente":patient, "form":patient_form_detail})