from django import forms
from django.contrib.auth.models import User

class CustomUserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'date_joined']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            for fields in self.fields:
                self.fields[fields].help_text = ''
                self.fields[fields].widget.attrs['readonly'] = True
                if self.fields[fields].widget.input_type == 'checkbox':
                    self.fields[fields].widget.attrs['onclick'] = 'return false;'