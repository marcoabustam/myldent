from django.db import models
from django.utils import timezone

# Create your models here.
class BaseAbstract(models.Model):
    create_date = models.DateField(default=timezone.now)
    update_date = models.DateField(default=timezone.now)

    class Meta:
        abstract = True
