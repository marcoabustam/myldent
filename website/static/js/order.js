function saleOrder() {
    var y = document.getElementById("saleOrder");
    if (y.style.display === "none") {
            y.style.display = "block";
    } else {
        y.style.display = "none";
    }
}

$( document ).ready(function() {
    total_lines = $("#id_order_lines-TOTAL_FORMS").val();
    recalc_total();
    $(".select").each(function (){
        $(this).select2();
    });
});

$(".select").mouseover(function(){
    if (!$(this).hasClass("select2-hidden-accessible")) {
        $(this).select2();
    }
});

$('input[type=radio][name=retirement_place]').change(function() {
    obs_value = $("#id_obs").val();
    if (this.value == '0') {
        $("#id_obs").val(obs_value.replace('CCU', 'PRO'));
    }
    else if (this.value == '1') {
        $("#id_obs").val(obs_value.replace('PRO', 'CCU'));
    }
});

$( "#id_other_obs" ).keyup(function() {
    obs_value =  $("#id_obs").val();
    o_obs =  ': ' + $( "#id_other_obs" ).val();
    $("#id_obs").val(obs_value.split(":")[0] + o_obs )

});


$("#id_charge_date").change(function(){
    values = this.value.split("-");
    day = values[2];
    month = values[1];

    obs_var = $("#id_obs").val()
    $("#id_obs").val( obs_var.split("-")[0] + "-" + day + "/" + month + ": " + obs_var.split(":")[1])

});

function recalc_total(){
    total_qty = 0
    total_pallets = 0
    for (i=0; i<=total_lines; i++){
        if(parseInt($("#id_order_lines-" + i + "-qty").val())){
            total_qty = total_qty + parseInt($("#id_order_lines-" + i + "-qty").val())
            total_pallets = total_pallets + parseInt($("#id_order_lines-" + i + "-qty_pallet").val())
            
            qty = $("#id_order_lines-" + i + "-qty").val()
            package_per_pallet = $("#id_order_lines-" + i + "-package_per_pallet").val()
            $("#id_order_lines-" + i + "-qty_pallet").val(Math.ceil(qty/package_per_pallet))
        }
    }
    $("#id_total_qty").val(total_qty);
    $("#id_current_charge").val(total_pallets);
    
}

$('#order_form').on('submit', function(e){
    if (e.originalEvent.submitter.id == 'submit'){
        total_charge = parseInt($("#id_current_charge").val());
        min_charge = parseInt($("#id_min_charge").val());
        if(total_charge < min_charge){
            Swal.fire({
                icon: 'info',
                title:'¡Advertencia!',
                html: "No ha cardo la cantidad mínima requerida para el centro de despacho seleccionado.<br>Debe ingresar más carga a la orden.",
            // 'success'
            });
            e.preventDefault();
            return false;       
        }
        return true;
    }
    return true;
  });