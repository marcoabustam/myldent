from django.urls import path
from . import views

urlpatterns = [
    path('', views.base, name="base"),
    path('home/', views.home, name="home"),
    path('mi_perfil/', views.view_profile, name="view_profile"),
    path('pagination/<int:patient_id>', views.pagination, name='pagination'),
]
