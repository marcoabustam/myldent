from django.urls import path
from . import views


urlpatterns = [
    path('', views.ask_odontogram, name='ask_odontogram'),
    path('new_odonto/', views.new_odontogram, name='new_odontogram'),
    path('odonto_<int:pk_mouth>/tooth_<str:nb_tooth>/', views.tooth_view, name='tooth'),
    path('odonto_<int:pk_mouth>/', views.update_odonto, name='update_odontogram'),
    path('odonto_code_<int:pk_mouth>/', views.view_odonto, name='odontogram_in_codes'),
]