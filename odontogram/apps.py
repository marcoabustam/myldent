from django.apps import AppConfig


class OdontogramConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'odontogram'
