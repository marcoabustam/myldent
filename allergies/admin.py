from django.contrib import admin
from .models import Allergies

class AllergiesAdmin(admin.ModelAdmin):
    list_display=('allergie_name',) 
    search_fields=('allergie_name',)
    list_filter=('allergie_name',)

admin.site.register(Allergies, AllergiesAdmin)
