from django.db import models
from django.utils import timezone

# Create your models here.
class Allergies(models.Model):

    allergie_name = models.CharField(max_length=50, null=True, blank=True, verbose_name='Alergia')
    create_date = models.DateField(default=timezone.now, verbose_name="Fecha de creación")
    update_date = models.DateField(default=timezone.now, verbose_name="Fecha de Actualización")

    class Meta: 
            verbose_name = "Alergia"
            verbose_name_plural = "Alergias"
    
    def __str__(self):
        return str(self.allergie_name) or ""