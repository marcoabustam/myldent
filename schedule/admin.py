from django.contrib import admin
from .models import Schedule

class ScheduleAdmin(admin.ModelAdmin):
    list_display=('name','date_on_calendar') 
    search_fields=('name','date_on_calendar')
    list_filter=('name','date_on_calendar')

admin.site.register(Schedule, ScheduleAdmin)
