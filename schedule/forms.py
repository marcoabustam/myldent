from django.http import JsonResponse
import requests
from django import forms
from django.shortcuts import render
from .models import Schedule


class ScheduleForm(forms.ModelForm):
    class Meta:
        model = Schedule
        fields = ['name', 'date_on_calendar']