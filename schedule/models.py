from django.db import models

class Schedule(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True, verbose_name='Nombre')
    date_on_calendar = models.CharField(max_length=50, null=True, blank=True, verbose_name='Fecha')

def __str__(self):
    return str(self.name) or ""