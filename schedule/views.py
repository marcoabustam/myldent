import datetime
import json
from django.http import JsonResponse
from django.shortcuts import render
import requests
from .forms import ScheduleForm
from .models import Schedule

def schedule_index(request):
    url_feriados = "https://apis.digital.gob.cl/fl/feriados"
    headers = requests.utils.default_headers()

    headers.update(
        {
            'User-Agent': 'My User Agent 1.0',
        }
    )
    try:
        response = requests.get(url_feriados, headers=headers)
        if response.status_code == 200:
            data = response.json()
            if data:
                data_dict = data[8]
                name = data_dict.get('nombre', '')
                date_on_calendar = data_dict.get('fecha', '') 
                form = ScheduleForm(initial={'name': name, 'date_on_calendar': date_on_calendar})
                today = datetime.date.today().strftime('%Y-%m-%d')
                return render(request, 'schedule_index.html', {'form': form, "data":data, "today":today})
        else:
            return JsonResponse({'error': 'Error al obtener los feriados'}, status=400)
    except requests.exceptions.RequestException as e:
        return JsonResponse({'error': str(e)}, status=500)