from django.contrib import admin
from .models import Diseases

class DiseasesAdmin(admin.ModelAdmin):
    list_display=('diseases_name',) 
    search_fields=('diseases_name',)
    list_filter=('diseases_name',)

admin.site.register(Diseases, DiseasesAdmin)