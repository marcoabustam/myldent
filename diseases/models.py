from django.db import models
from django.utils import timezone


# Create your models here.
class Diseases(models.Model):
    
    diseases_name = models.CharField(max_length=50, null=True, blank=True, verbose_name='Nombre')
    create_date = models.DateField(default=timezone.now, verbose_name="Fecha de creación")
    update_date = models.DateField(default=timezone.now, verbose_name="Fecha de Actualización")

    class Meta: 
            verbose_name = "Enfermedad"
            verbose_name_plural = "Enfermedades"
    
    def __str__(self):
        return str(self.diseases_name) or ""